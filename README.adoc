= Scripts
For certain scripts I've added a "requires" section to specify software which is less likely to be installed by default that the script uses. Most of these can be changed with alternatives by choice if willing to edit the script.
:toc:

== link:bin/askpass[askpass]

Prompts for sudo password - using Zenity GUI only when using display server. Meant to be used with addition to the sudo.conf:

[source, conf]
----
Path askpass /path/to/askpass
----

With this, sudo -A can be used to prompt this script.

.Requires:

* https://gitlab.gnome.org/GNOME/zenity[zenity]

== link:bin/auru[auru]

Arch Linux AUR updater. Edit path to your AUR repositories in the script before running. For every updated repository, a file selector GUI will prompt to the delete outdated files.

.Requires:

* https://gitlab.gnome.org/GNOME/zenity[zenity]

== link:bin/bookmark[bookmark]

Uses dmenu to browse and open URL's in your browser saved in a textfile (default in ~/.config/scripts/bookmarks).

.Requires:

* https://git.suckless.org/dmenu/[dmenu]

== link:bin/coin[coin]

Used to grab cryptocurrency data, including market price. The data itself is powered by CoinGecko API.
This script could easily be chained with https://github.com/dunst-project/dunst[dunst] to have the output as notification. 

.Requires:

* https://github.com/stedolan/jq[jq]

image::docs/coin-in-dunst.png[coin-in-dunst.png]

== link:bin/decr[decr]

Decrypts a GPG encrypted TAR file.

== link:bin/dict[dict]

Uses https://dictionaryapi.dev/[Free dictionary API] to retrieve word definitions in JSON.

.Requires:

* https://github.com/stedolan/jq[jq]

== link:bin/encr[encr]

Archives a given file or directory as a TAR file and encrypts it with GPG using symmetric or asymmetric encryption. By default it uses symmetric encryption and looks for encrypted
file "~/.sym-pwd.gpg" to use its content as password.

== link:bin/gitc[gitc]

Adds, commits and pushes git changes to all remotes after confirmation. The "pushall" option should first be added to your gitconfig:

[source,conf]
----
[alias]
	pushall = !git remote | xargs -L1 git push
----

== link:bin/htmlconv[htmlconv]

Generates html for asciidoc and markdown files and opens in browser or refreshes if current tab has
the html filename in URL.

.Requires:

* https://github.com/astrand/xclip[xclip]
* https://github.com/jordansissel/xdotool[xdotool]
* https://github.com/asciidoctor/asciidoctor.git[asciidoctor]
* https://github.com/mity/md4c.git[md4c]

== link:bin/mpvd[mpvd]

Uses dmenu to browse through music directory, including option to shuffle when choosing to play a directory.

.Requires:

* https://git.suckless.org/dmenu/[dmenu]
* https://github.com/mpv-player/mpv[mpv]

== link:bin/mpvpop[mpvpop]

Uses mpv socket to be able to update and grab info of currently played mpv file and/or playlist.

Can be used using '--input-ipc-server=/tmp/mpvsocket' mpv argument. If you always want this active than you are better off adding to mpv.conf:

[source,conf]
----
input-ipc-server=/tmp/mpvsocket
----

.Requires:

* https://github.com/mpv-player/mpv[mpv]
* https://github.com/dunst-project/dunst[dunst]

== link:bin/otp[otp]

Lists files safed under the "~/.password-store/totp/" directory in dmenu or as argument and generates TOTP code based on chosen file.

.Requires:

* https://git.zx2c4.com/password-store/[pass]
* https://github.com/tadfisher/pass-otp[pass-otp]
* https://git.suckless.org/dmenu/[dmenu]

== link:bin/pclip[pclip]

Used to show all clipboard contents in dmenu and allows you to select one which will be typed out.

.Requires:

* https://git.suckless.org/dmenu/[dmenu]
* https://github.com/astrand/xclip[xclip]
* https://github.com/jordansissel/xdotool[xdotool]

== link:bin/pwu[pwu]

Lists files safed under the "~/.password-store/" directory in dmenu or as argument. The chosen file gets opened using pass followed by:

* password (first line) get saved in clipboard
* username (second line) gets saved in primary using xclip
* URL (third line) gets opened in default browser using xdg-open

Only first line is mandatory for this script to be useful, usernames en URL's can be added to files where and when preferred. If there exists a file under the the "otp/" directory
for the same name as the chosen file then pass-otp will be used to copy the OTP to the clipboard after a prompt.

.Requires:

* https://git.zx2c4.com/password-store/[pass]
* https://git.suckless.org/dmenu/[dmenu]
* https://github.com/astrand/xclip[xclip]

== link:bin/sinstaller[sinstaller]

Installation and patching helper for https://git.suckless.org/[suckless] software. Adjust PROGRAMS and PATCHES arrays to your liking before running.

.Requires:

* https://gitlab.gnome.org/GNOME/zenity[zenity]

== link:bin/radio[radio]

Uses dmenu to browse and play online radio stations saved in a textfile (default in ~/.config/scripts/radiostations). The textfile should consist of a searchname and the URL separated by a semicolon (;) and with a new line for each new station.

.Requires:

* https://git.suckless.org/dmenu/[dmenu]
* https://github.com/mpv-player/mpv[mpv]

== link:bin/ugamma[ugamma]

Helper script to update X.org gamma value.

.Requires:

* https://github.com/dunst-project/dunst[dunst]
* https://gitlab.freedesktop.org/xorg/app/xgamma[xorg-xgamma]
* https://github.com/lcn2/calc[calc]

== link:bin/wta[wta]

Looks up words to add to .csv file after confirmation. To be used as helper for other programs such as link:https://git.wimdupont.com/wordstudent[wordstudent].

.Requires:

* link:bin/dict[dict]

== link:bin/yt[yt]

Uses mpv and youtube-dl (or forks such as yt-dlp) to easily look up videos from command-line with an audio-only option and an option to specify the amount of results.

.Requires:

* https://github.com/mpv-player/mpv[mpv]
* https://github.com/ytdl-org/youtube-dl[youtube-dl]

== link:bin/ytd[ytd]

Uses yt-dlp to easily download videos from youtube. Has options for audio-only files from youtube, can also download playlists, and allows for searches on either URL's or keywords.

.Requires:

* https://github.com/yt-dlp/yt-dlp[yt-dlp]

== link:bin/ytd-album[ytd-album]

Used to download full albums by playlist. The script creates a directory with the bandname followed by a directory with albumname in which the playlist is saved as mp3 files.
The downloaded audio files also get tagged by id3 to make them easy to sort by on devices such as mp3-players.

.Requires:

* https://github.com/yt-dlp/yt-dlp[yt-dlp]
* https://git.ffmpeg.org/ffmpeg.git[ffmpeg]
* https://github.com/squell/id3[id3]
