#!/usr/bin/env bash

source ~/.config/scripts/properties

shopt -s nullglob globstar

typeit=0

while getopts ":t" arg; do
  case $arg in
    t) typeit=1
  esac
done

shift $((OPTIND-1))

value=$1

if [[ -n $WAYLAND_DISPLAY ]]; then
	dmenu=dmenu-wl
	xdotool="ydotool type --file -"
elif [[ -n $DISPLAY ]]; then
	dmenu=dmenu
	xdotool="xdotool type --clearmodifiers --file -"
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi

prefix=${PASSWORD_STORE_DIR-~/.password-store}
password_files=( "$prefix"/otp/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )

function use_arg_file () {
	password=$1
	if [[ $typeit -eq 0 ]]; then
		pass otp -c "$password" 2>/dev/null
	else
		pass otp "$password" | { IFS= read -r pass; printf %s "$pass"; } | $xdotool
	fi
	exit;
}

if [ -n "${value}" ]; then
	shift
	let i=0
	for file in "${password_files[@]}" 
	do
		if [[ "$file" =~ "${value}" ]] ; then
			argFile="${file}"
			let i++
		fi
	done

	if [ "$i" == 1 ]; then
		use_arg_file "${argFile}"
	else
		if [ "$i" == 0 ]; then
			echo "Found no otp files for \"${value}\"."
		fi
		if [ "$i" -gt 1 ]; then
			echo "Found more than one possible otp file for \"${value}\"."
		fi
	fi
else
	password=$(printf '%s\n' "${password_files[@]}" | "$dmenu" -fn "$dmenu_font" -i )
	[[ -n $password ]] || exit
	
	use_arg_file "${password}"
fi
